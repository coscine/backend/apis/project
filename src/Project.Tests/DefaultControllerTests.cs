﻿using Coscine.Api.Project.Controllers;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.JwtHandler;
using Coscine.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace Coscine.Api.Project.Tests
{
    public abstract class DefaultControllerTests<T> where T : Controller
    {
        protected T Controller { get; }

        protected readonly List<Coscine.Database.DataModel.Project> Projects = new List<Coscine.Database.DataModel.Project>();
        protected readonly List<User> Users = new List<User>();
        protected readonly List<ProjectRole> ProjectRoles = new List<ProjectRole>();
        protected readonly List<ResourceType> ResourceTypes = new List<ResourceType>();
        protected readonly List<Resource> Resources = new List<Resource>();
        protected readonly List<ResourceDiscipline> ResourceDisciplines = new List<ResourceDiscipline>();

        protected Discipline Discipline { get; set; }
        protected string Organization { get; set; }
        protected Visibility Visibility { get; set; }
        protected License License { get; set; }
        protected RdsresourceType RdsResourceType { get; set; }

        protected int Previous;

        protected DefaultControllerTests(T controller)
        {
            Controller = controller;
        }

        private static ILogger<ProjectController> projectLogger = null;

        private static ILogger<ProjectController> ProjectLogger
        {
            get
            {
                if (projectLogger == null)
                {
                    CoscineLoggerConfiguration.SetConfig();
                    projectLogger = new NullLogger<ProjectController>();
                }
                return projectLogger;
            }
        }

        [OneTimeSetUp]
        public void Setup()
        {
            DisciplineModel disciplineModel = new DisciplineModel();
            Discipline = new Discipline()
            {
                DisplayNameDe = "TestDiscipline",
                DisplayNameEn = "TestDiscipline",
                Url = "http://rwth-aachen.de"
            };
            disciplineModel.Insert(Discipline);

            Organization = "https://www.rwth-aachen.de/22000";

            VisibilityModel visibilityModel = new VisibilityModel();
            Visibility = visibilityModel.GetWhere((visibility) => visibility.DisplayName == "Public");

            LicenseModel licenseModel = new LicenseModel();
            License = new License()
            {
                DisplayName = "MIT"
            };
            licenseModel.Insert(License);

            ProjectModel projectModel = new ProjectModel();
            Previous = projectModel.GetAll().ToArray().Length;

            UserModel userModel = new UserModel();

            var user = new User()
            {
                Id = Guid.NewGuid(),
                DisplayName = "TestUser",
                EmailAddress = null,
            };

            userModel.Insert(user);
            Users.Add(user);

            FakeControllerContext(user);

            var project = new Coscine.Database.DataModel.Project()
            {
                Description = "Description",
                ProjectName = "TestProject",
                DisplayName = "TestProject",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddYears(1),
                Keywords = "Test1;Test2",

                GrantId = "testid",
                PrincipleInvestigators = "TestInvestigator",

                VisibilityId = Visibility.Id,
                Slug = Guid.NewGuid().ToString()
            };

            projectModel.Insert(project);
            var projectRole = projectModel.SetOwner(project, user);
            ProjectRoles.Add(projectRole);

            ProjectDisciplineModel projectDisciplineModel = new ProjectDisciplineModel();
            projectDisciplineModel.Insert(new ProjectDiscipline()
            {
                DisciplineId = Discipline.Id,
                ProjectId = project.Id
            });

            ProjectInstituteModel projectInstituteModel = new ProjectInstituteModel();
            projectInstituteModel.Insert(new ProjectInstitute()
            {
                OrganizationUrl = Organization,
                ProjectId = project.Id
            });

            Projects.Add(projectModel.GetById(project.Id));

            var project2 = new Coscine.Database.DataModel.Project()
            {
                Description = "Description2",
                ProjectName = "TestProject",
                DisplayName = "TestProject2",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddYears(1),
                Keywords = "Test1;Test2",

                GrantId = "testid",
                PrincipleInvestigators = "TestInvestigator",

                VisibilityId = Visibility.Id,
                Slug = Guid.NewGuid().ToString()
            };
            projectModel.Insert(project2);
            Projects.Add(projectModel.GetById(project2.Id));

            ResourceTypeModel resourceTypeModel = new ResourceTypeModel();
            var resourceType = resourceTypeModel.GetWhere((dbResourceType) => dbResourceType.DisplayName == "rds");

            RDSResourceTypeModel rdsResourceTypeModel = new RDSResourceTypeModel();
            RdsResourceType = new RdsresourceType()
            {
                BucketName = "c"
            };
            rdsResourceTypeModel.Insert(RdsResourceType);

            ResourceModel resourceModel = new ResourceModel();
            var resource = new Resource()
            {
                DisplayName = "ResourceTest1",
                ResourceName = "ResourceTest1",
                Keywords = "ResourceTest1",
                UsageRights = "ResourceTest1",
                TypeId = resourceType.Id,
                Type = resourceType,

                Visibility = Visibility,
                VisibilityId = Visibility.Id,
                LicenseId = License.Id,

                ResourceTypeOptionId = RdsResourceType.Id
            };
            resourceModel.Insert(resource);
            projectModel.AddResource(project, resource);
            Resources.Add(resource);

            resourceType = resourceTypeModel.GetWhere((dbResourceType) => dbResourceType.DisplayName == "gitlab");
            var resource2 = new Resource()
            {
                DisplayName = "ResourceTest2",
                ResourceName = "ResourceTest2",
                Keywords = "ResourceTest2",
                UsageRights = "ResourceTest2",
                TypeId = resourceType.Id,
                Type = resourceType,

                Visibility = Visibility,
                VisibilityId = Visibility.Id,
                LicenseId = License.Id
            };
            resourceModel.Insert(resource2);
            projectModel.AddResource(project2, resource2);
            Resources.Add(resource2);

            ResourceDisciplineModel resourceDisciplineModel = new ResourceDisciplineModel();
            ResourceDiscipline resourceDiscipline = new ResourceDiscipline()
            {
                DisciplineId = Discipline.Id,
                ResourceId = resource.Id
            };
            resourceDisciplineModel.Insert(resourceDiscipline);
            ResourceDisciplines.Add(resourceDiscipline);
            resourceDiscipline = new ResourceDiscipline()
            {
                DisciplineId = Discipline.Id,
                ResourceId = resource2.Id
            };
            resourceDisciplineModel.Insert(resourceDiscipline);
            ResourceDisciplines.Add(resourceDiscipline);
        }

        protected void FakeControllerContext(User user, Stream stream = null)
        {
            var request = new Mock<HttpRequest>();

            JWTHandler jwtHandler = new JWTHandler(Program.Configuration);
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "userId", user.Id.ToString() }
            };

            var sharePointSite = Program.Configuration.GetStringAndWait("coscine/local/sharepoint/additional/url");

            request.SetupGet(x => x.Headers).Returns(
                new HeaderDictionary {
                    {"X-Requested-With", "XMLHttpRequest"},
                    {"Authorization", "Bearer " + jwtHandler.GenerateJwtToken(values)},
                    {"Referer", sharePointSite }
                }
            );

            var context = new Mock<HttpContext>();
            context.SetupGet(x => x.Request).Returns(request.Object);

            var claimsPrincipal = new Mock<ClaimsPrincipal>();
            Claim claim = new Claim("userId", user.Id.ToString());
            var list = new List<Claim>
            {
                claim
            };
            context.SetupGet(x => x.User).Returns(claimsPrincipal.Object);
            context.Setup(x => x.User.FindFirst("userId")).Returns(claim);
            context.SetupGet(x => x.User.Claims).Returns(list);

            if (stream != null)
            {
                context.SetupGet(x => x.Request.Method).Returns("POST");
                context.SetupGet(x => x.Request.HttpContext).Returns(context.Object);
                context.SetupGet(x => x.Request.Body).Returns(stream);
                context.SetupGet(x => x.Request.ContentLength).Returns(stream.Length);
                context.SetupGet(x => x.Request.ContentType).Returns("application/json");
            }

            var actionDescriptor = new Mock<ControllerActionDescriptor>();

            Controller.ControllerContext = new ControllerContext(new ActionContext(context.Object, new RouteData(), actionDescriptor.Object));
        }

        [OneTimeTearDown]
        public void End()
        {
            ProjectRoleModel projectRoleModel = new ProjectRoleModel();
            foreach (var projectRole in ProjectRoles)
            {
                projectRoleModel.Delete(projectRole);
            }
            ProjectResourceModel projectResourceModel = new ProjectResourceModel();
            foreach (var projectResource in projectResourceModel.GetAllWhere((projectResource) => projectResource.ResourceId == Resources[0].Id || projectResource.ResourceId == Resources[1].Id))
            {
                projectResourceModel.Delete(projectResource);
            }
            ProjectController projectController = new ProjectController(ProjectLogger);
            foreach (var project in Projects)
            {
                projectController.DeleteProject(project, true, false);
            }
            UserModel userModel = new UserModel();
            foreach (var user in Users)
            {
                userModel.Delete(user);
            }
            ResourceDisciplineModel resourceDisciplineModel = new ResourceDisciplineModel();
            foreach (var resourceDiscipline in ResourceDisciplines)
            {
                resourceDisciplineModel.Delete(resourceDiscipline);
            }
            ResourceModel resourceModel = new ResourceModel();
            foreach (var resource in Resources)
            {
                resourceModel.DeleteResource(resource);
            }
            ResourceTypeModel resourceTypeModel = new ResourceTypeModel();
            foreach (var resourceType in ResourceTypes)
            {
                resourceTypeModel.Delete(resourceType);
            }

            DisciplineModel disciplineModel = new DisciplineModel();
            disciplineModel.Delete(Discipline);

            LicenseModel licenseModel = new LicenseModel();
            licenseModel.Delete(License);

            RDSResourceTypeModel rdsResourceTypeModel = new RDSResourceTypeModel();
            rdsResourceTypeModel.Delete(RdsResourceType);
        }
    }
}