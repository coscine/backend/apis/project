﻿using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Project.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a license object.
    /// </summary>
    [Authorize]
    public class LicenseController : Controller
    {
        private readonly LicenseModel _licenseModel;

        /// <summary>
        /// LicenseController constructor specifying a LicenseModel
        /// </summary>
        public LicenseController()
        {
            _licenseModel = new LicenseModel();
        }

        /// <summary>
        /// Returns all available licenses
        /// </summary>
        /// <returns>All Licenses</returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<LicenseObject>> Index()
        {
            return Ok(_licenseModel.GetAll()
                .Select((license) => new LicenseObject(license.Id, license.DisplayName)));
        }
    }
}