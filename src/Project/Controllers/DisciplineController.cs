﻿using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Project.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a discipline object.
    /// </summary>
    [Authorize]
    public class DisciplineController : Controller
    {
        private readonly DisciplineModel _disciplineModel;

        /// <summary>
        /// DisciplineController constructor specifying a DisciplineModel
        /// </summary>
        public DisciplineController()
        {
            _disciplineModel = new DisciplineModel();
        }

        /// <summary>
        /// Returns all available disciplines
        /// </summary>
        /// <returns>All Disciplines</returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<DisciplineObject>> Index()
        {
            return Ok(_disciplineModel.GetAll()
                .OrderBy(discipline => discipline.DisplayNameDe.Substring(discipline.DisplayNameDe.Length - 3))
                .Select((discipline) => new DisciplineObject(discipline.Id, discipline.Url, discipline.DisplayNameDe, discipline.DisplayNameEn)));
        }
    }
}