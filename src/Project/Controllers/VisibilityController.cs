﻿using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Project.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a visibility object
    /// </summary>
    ///
    [Authorize]
    public class VisibilityController : Controller
    {
        private readonly VisibilityModel _visibilityModel;

        /// <summary>
        /// VisibilityController specifying a VisibilityModel
        /// </summary>
        public VisibilityController()
        {
            _visibilityModel = new VisibilityModel();
        }

        /// <summary>
        /// Returns all available visibilities
        /// </summary>
        /// <returns>All Visibilities</returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<VisibilityObject>> Index()
        {
            return Ok(_visibilityModel.GetAll()
                .Select((visibility) => new VisibilityObject(visibility.Id, visibility.DisplayName)));
        }
    }
}