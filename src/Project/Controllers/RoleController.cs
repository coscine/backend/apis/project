﻿using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Project.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a role object
    /// </summary>
    [Authorize]
    public class RoleController : Controller
    {
        private readonly RoleModel _roleModel;

        /// <summary>
        /// RoleController specifying a RoleModel
        /// </summary>
        public RoleController()
        {
            _roleModel = new RoleModel();
        }

        /// <summary>
        /// Returns all available roles
        /// </summary>
        /// <returns>All Roles </returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<RoleObject>> Index()
        {
            return Ok(_roleModel.GetAll()
                .Select((role) => new RoleObject(role.Id, role.DisplayName)));
        }
    }
}