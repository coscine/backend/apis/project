using Coscine.Action;
using Coscine.Action.EventArgs;
using Coscine.Action.Utils;
using Coscine.Api.Project.ParameterObjects;
using Coscine.ApiCommons;
using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.Database.Util;
using Coscine.Logging;
using Coscine.Metadata;
using Coscine.ResourceTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.Api.Project.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a project object.
    /// </summary>
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly Authenticator _authenticator;
        private readonly ProjectModel _projectModel;
        private readonly IConfiguration _configuration;
        private readonly Emitter _emitter;
        private readonly ActivatedFeaturesModel _activatedFeaturesModel;
        private readonly ProjectRoleModel _projectRoleModel;
        private readonly ProjectQuotaModel _projectQuotaModel;
        private readonly ResourceTypeModel _resourceTypeModel;
        private readonly CoscineLogger _coscineLogger;
        private readonly VisibilityModel _visibilityModel;
        private readonly InvitationModel _invitationModel;
        private readonly RoleModel _roleModel;
        private readonly UserModel _userModel;
        private readonly RdfStoreConnector _rdfStoreConnector;

        /// <summary>
        /// ProjectController constructor
        /// </summary>
        /// <param name="logger">Logger</param>
        public ProjectController(ILogger<ProjectController> logger)
        {
            _authenticator = new Authenticator(this, Program.Configuration);
            _configuration = Program.Configuration;
            _projectModel = new ProjectModel();
            _emitter = new Emitter(_configuration);
            _activatedFeaturesModel = new ActivatedFeaturesModel();
            _projectRoleModel = new ProjectRoleModel();
            _resourceTypeModel = new ResourceTypeModel();
            _projectQuotaModel = new ProjectQuotaModel();
            _coscineLogger = new CoscineLogger(logger);
            _visibilityModel = new VisibilityModel();
            _rdfStoreConnector = new RdfStoreConnector(Program.Configuration.GetString("coscine/local/virtuoso/additional/url"));
            _invitationModel = new InvitationModel();
            _roleModel = new RoleModel();
            _userModel = new UserModel();
        }

        /// <summary>
        /// Returns all available projects (including sub projects)
        /// </summary>
        /// <returns>OK</returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<ProjectObject>> Index()
        {
            var user = _authenticator.GetUser();
            var result = _projectModel.GetWithAccess(user, UserRoles.Member, UserRoles.Owner).ToList()
                .Select((project) => _projectModel.CreateReturnObjectFromDatabaseObject(project))
                .OrderBy(element => element.DisplayName);

            return Ok(result);
        }

        /// <summary>
        /// Retrieves all top level projects
        /// </summary>
        /// <returns>OK</returns>
        [Route("[controller]/-/topLevel")]
        public ActionResult<IEnumerable<ProjectObject>> GetTopLevelProjects()
        {
            var user = _authenticator.GetUser();
            var projects = _projectModel.GetTopLevelWithAccess(user, UserRoles.Member, UserRoles.Owner, UserRoles.Guest).ToList()
                .Select((project) => _projectModel.CreateReturnObjectFromDatabaseObject(project))
                .OrderBy(element => element.DisplayName);

            if (Request.Query != null && Request.Query["noanalyticslog"] != "true")
            {
                LogAnalyticsViewHome(projects.Select(x => x.Id.ToString()).ToList());
            }

            return Ok(projects);
        }

        /// <summary>
        /// This returns the project if the user has access to it
        /// </summary>
        /// <param name="id">Id of the project</param>
        /// <returns>OK or status code 401</returns>
        [HttpGet("[controller]/{id}")]
        public ActionResult<ProjectObject> Get(Guid id)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(id);

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Coscine Basis: View Projects
            if (_projectModel.HasAccess(user, project, UserRoles.Member, UserRoles.Owner, UserRoles.Guest))
            {
                var subProjectModel = new SubProjectModel();
                var subProjectRel = subProjectModel.GetAllWhere((subProject) => subProject.SubProjectId == project.Id && !project.Deleted);

                var parentProjectRelation = subProjectRel.FirstOrDefault();

                // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
                // - Project: View Subprojects (no inheritance, invited seperately) 
                if (parentProjectRelation != null && _projectModel.HasAccess(user, parentProjectRelation.ProjectId, UserRoles.Member, UserRoles.Owner, UserRoles.Guest))
                {
                    return Ok(_projectModel.CreateReturnObjectFromDatabaseObject(project, parentProjectRelation.ProjectId));
                }
                return Ok(_projectModel.CreateReturnObjectFromDatabaseObject(project));
            }
            else
            {
                return Unauthorized($"User has no access to a project with id: {id}");
            }
        }

        /// <summary>
        /// This returns the project if the user has access to it
        /// </summary>
        /// <param name="slug">Slug of the project</param>
        /// <returns>OK or status code 401</returns>
        [HttpGet("[controller]/slug/{slug}")]
        public ActionResult<ProjectObject> GetBySlug(string slug)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetBySlug(slug);

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Coscine Basis: View Projects // Project: View Subprojects (no inheritance, invited seperately) 
            if (_projectModel.HasAccess(user, project, UserRoles.Member, UserRoles.Owner, UserRoles.Guest))
            {
                var subProjectModel = new SubProjectModel();
                var subProjectRel = subProjectModel.GetAllWhere((subProject) => subProject.SubProjectId == project.Id && !project.Deleted);

                var parentProjectRelation = subProjectRel.FirstOrDefault();

                // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
                // - Coscine Basis: View Projects // Project: View Subprojects (no inheritance, invited seperately) 
                if (parentProjectRelation != null && _projectModel.HasAccess(user, parentProjectRelation.ProjectId, UserRoles.Member, UserRoles.Owner, UserRoles.Guest))
                {
                    return Ok(_projectModel.CreateReturnObjectFromDatabaseObject(project, parentProjectRelation.ProjectId));
                }
                return Ok(_projectModel.CreateReturnObjectFromDatabaseObject(project));
            }
            else
            {
                return Unauthorized($"User has no access to a project with slug: {slug}");
            }
        }

        /// <summary>
        /// Gets all resources in a project
        /// </summary>
        /// <param name="id">Id of the project</param>
        /// <returns>JSON object or status code 401</returns>
        [HttpGet("[controller]/{id}/resources")]
        public ActionResult<IEnumerable<ResourceObject>> GetResources(Guid id)
        {
            var project = _projectModel.GetById(id);
            var user = _authenticator.GetUser();

            var resourceModel = new ResourceModel();
            var resourceTypeModel = new ResourceTypeModel();

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Resource: View Resource (RCV, Metadatamanager)
            if (_projectModel.HasAccess(user, project, UserRoles.Member, UserRoles.Owner, UserRoles.Guest))
            {
                var resources = resourceModel.GetAllWhere((resource) =>
                        (from projectResource in resource.ProjectResources
                         where projectResource.ProjectId == project.Id
                         select projectResource).Any())
                        .Select(resource => ResourceTypes.Helpers.CreateResourceReturnObject(resource))
                        .Select(resource => HideSensitiveInformation(user, project, resource))
                        .OrderBy(element => element.DisplayName);
                
                if (Request.Query != null && Request.Query["noanalyticslog"] != "true")
                {
                    var projectObject = _projectModel.CreateReturnObjectFromDatabaseObject(_projectModel.GetById(project.Id));
                    LogAnalyticsViewProject(project, resources.ToList(), projectObject.Disciplines, projectObject.Organizations, user);
                }
                return Ok(resources);
            }
            else
            {
                return Unauthorized($"User has no access to a project with id: {id}");
            }
        }

        private ResourceObject HideSensitiveInformation(User user, Database.DataModel.Project project, ResourceObject resourceObject)
        {
            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Resource: View Resource (RCV, Metadatamanager) - BUT hide resource type options!
            if (_projectModel.HasAccess(user, project, UserRoles.Guest))
            {
                // From resource.resourceTypeOption hide all sensitive keys listed bellow that a user should not have access to, based on their role
                resourceObject.ResourceTypeOption = null;
            }
            return resourceObject;
        }

        /// <summary>
        /// Updates a project
        /// </summary>
        /// <param name="id">Id of the project</param>
        /// <param name="projectObject">A Project Object</param>
        /// <returns>OK or status code 401</returns>
        [HttpPost("[controller]/{id}")]
        public IActionResult Update(Guid id, [FromBody] ProjectObject projectObject)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(id);

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Change Settings (project, user, quota)
            if (_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                LogAnalyticsEditProject(project, _projectModel.GetMetadataCompleteness(projectObject), projectObject.Disciplines, projectObject.Organizations, user);
                return Ok(_projectModel.UpdateByObject(project, projectObject));
            }
            else
            {
                return Unauthorized("The user is not authorized to perform an update on the selected project!");
            }
        }

        /// <summary>
        /// Deletes a project
        /// </summary>
        /// <param name="id">Id of the project</param>
        /// <returns>JSON object or status code 401</returns>
        [HttpDelete("[controller]/{id}")]
        public IActionResult Delete(Guid id)
        {
            var user = _authenticator.GetUser();
            var project = _projectModel.GetById(id);

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Change Settings (project, user, quota)
            if (_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                var projectObject = _projectModel.CreateReturnObjectFromDatabaseObject(_projectModel.GetById(project.Id));
                LogAnalyticsDeleteProject(project, projectObject.Disciplines, projectObject.Organizations, user);
                DeleteProject(project);
                return Ok(_projectModel.CreateReturnObjectFromDatabaseObject(project));
            }
            else
            {
                return Unauthorized("The user is not authorized to perform an update on the selected project!");
            }
        }

        /// <summary>
        /// Deletes the project
        /// </summary>
        /// <param name="project">Project</param>
        /// <param name="isHard">isHard</param>
        /// <param name="propagateAction">propagate Action</param>
        public void DeleteProject(Database.DataModel.Project project, bool isHard = false, bool propagateAction = true)
        {
            var subProjectModel = new SubProjectModel();
            foreach (var subProject in subProjectModel.GetAllWhere(
                (subProject) => subProject.ProjectId == project.Id
                                && (!subProject.SubProjectNavigation.Deleted || isHard)
            ))
            {
                Database.DataModel.Project subProjectObject;
                if (isHard)
                {
                    subProjectObject = _projectModel.GetByIdIncludingDeleted(subProject.SubProjectId);
                    subProjectModel.Delete(subProject);
                }
                else
                {
                    subProjectObject = _projectModel.GetById(subProject.SubProjectId);
                }
                DeleteProject(subProjectObject, isHard, propagateAction);
            }

            foreach (var subProject in subProjectModel.GetAllWhere((subProject) => subProject.SubProjectId == project.Id))
            {
                if (isHard)
                {
                    subProjectModel.Delete(subProject);
                }
            }

            if (isHard)
            {
                var projectResourceModel = new ProjectResourceModel();
                var resourceModel = new ResourceModel();
                var resourceTypeModel = new ResourceTypeModel();
                foreach (var projectResource in projectResourceModel.GetAllWhere((projectResource) => projectResource.ProjectId == project.Id))
                {
                    var resource = resourceModel.GetById(projectResource.ResourceId);
                    var resourceTypeOptions = resourceModel.GetResourceTypeOptions(projectResource.ResourceId);
                    var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);
                    resourceTypeDefinition.DeleteResource(projectResource.ResourceId.ToString(), resourceTypeOptions);
                    projectResourceModel.Delete(projectResource);
                    resourceModel.Delete(resource);
                }

                var projectRoleModel = new ProjectRoleModel();
                foreach (var projectRole in projectRoleModel.GetAllWhere((projectRole) => projectRole.ProjectId == project.Id))
                {
                    projectRoleModel.Delete(projectRole);
                }

                var projectDisciplineModel = new ProjectDisciplineModel();
                foreach (var projectDiscipline in projectDisciplineModel.GetAllWhere((projectDiscipline) => projectDiscipline.ProjectId == project.Id))
                {
                    projectDisciplineModel.Delete(projectDiscipline);
                }

                var projectInstituteModel = new ProjectInstituteModel();
                foreach (var projectInstitute in projectInstituteModel.GetAllWhere((projectInstitute) => projectInstitute.ProjectId == project.Id))
                {
                    projectInstituteModel.Delete(projectInstitute);
                }

                foreach (var projectQuota in _projectQuotaModel.GetAllWhere((Quota) => Quota.ProjectId == project.Id))
                {
                    _projectQuotaModel.Delete(projectQuota);
                }

                foreach (var invitation in _invitationModel.GetAllWhere((x) => x.Project == project.Id))
                {
                    _invitationModel.Delete(invitation);
                }

                _activatedFeaturesModel.DeactivateAllFeatures(project);

                if (propagateAction)
                {
                    _emitter.EmitProjectDelete(new ProjectEventArgs(_configuration)
                    {
                        Project = project
                    });
                }

                _projectModel.HardDelete(project);
            }
            else
            {
                _projectModel.Delete(project);
            }
        }

        /// <summary>
        /// Creates a new project
        /// </summary>
        /// <param name="projectObject">The project to create.</param>
        /// <returns>A JSON representation of the created project.</returns>
        /// <response code="200">The project was created successfully.</response>
        /// <response code="401">The user does not have sufficient permissions to create a project.</response>
        /// <response code="422">The project object is invalid or contains invalid data.</response>
        [HttpPost("[controller]")]
        public IActionResult Store([FromBody] ProjectObject projectObject)
        {
            // ------
            // This is the wrong place to do that, but we have to live with it. For the APIv2 consider moving that to a validation attribute.
            // Also consider using the following Regex:
            // (http|https):\/\/ror\.org\/[a-zA-Z0-9]+
            if (projectObject.Organizations.Any(o => o.Url.Contains('#')))
                return UnprocessableEntity("The URL of the organization cannot contain the '#' character. Only top-level organizations are allowed.");
            // ------
            var user = _authenticator.GetUser();

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Create Subprojects
            if (projectObject.ParentId != new Guid() && !_projectModel.HasAccess(user, _projectModel.GetById(projectObject.ParentId), UserRoles.Member, UserRoles.Owner))
            {
                return Unauthorized("User is not allowed to create sub-projects in the selected project!");
            }

            var defaultQuotas = _rdfStoreConnector.GetQuotaDefault(user.Id.ToString());
            var projectQuotas = defaultQuotas.Select(defaulQuota =>
            {
                var resourceType = _resourceTypeModel.GetWhere(resourceType => resourceType.DisplayName == defaulQuota.ResourceType);

                return new ProjectQuota
                {
                    MaxQuota = defaulQuota.DefaultMaxQuota,
                    Quota = defaulQuota.DefaultQuota,
                    ResourceTypeId = resourceType.Id
                };
            });

            var project = _projectModel.StoreFromObject(projectObject, user, projectQuotas);

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Create Subprojects
            if (projectObject.ParentId != new Guid() && _projectModel.HasAccess(user, _projectModel.GetById(projectObject.ParentId), UserRoles.Member, UserRoles.Owner))
            {
                var subProjectModel = new SubProjectModel();
                subProjectModel.LinkSubProject(projectObject.ParentId, project.Id);
            }

            _emitter.EmitProjectCreate(new ProjectEventArgs(_configuration)
            {
                Project = project,
                ProjectOwner = user
            });

            LogAnalyticsAddProject(project, _projectModel.GetMetadataCompleteness(projectObject), projectObject.Disciplines, projectObject.Organizations, user);

            return Ok(_projectModel.CreateReturnObjectFromDatabaseObject(project));
        }

        /// <summary>
        /// List all invitations of a project.
        /// </summary>
        /// <param name="projectId">Id of the project</param>
        /// <returns>List of invitations</returns>
        [HttpGet("[controller]/invitation/list/{projectId}")]
        public ActionResult<IEnumerable<InvitationReturnObject>> ListInvitations(Guid projectId)
        {
            var project = _projectModel.GetById(projectId);

            if (project == null)
            {
                return NotFound($@"The project ""{projectId}"" was not found.");
            }

            var user = _authenticator.GetUser();

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: View Settings (project, user, quota)
            if (!_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                return Unauthorized("You are not an owner of the project.");
            }

            var invitations = _invitationModel.GetAllWhere(x => x.Project == projectId)
                .Select(x => new InvitationReturnObject
                {
                    Id = x.Id,
                    Expiration = x.Expiration,
                    Issuer = x.Issuer,
                    ProjectId = x.Project,
                    RoleId = x.Role,
                    UserMail = x.InviteeEmail
                });

            return new ActionResult<IEnumerable<InvitationReturnObject>>(invitations);
        }

        /// <summary>
        /// Creation of an Application Profile.
        /// </summary>
        /// <param name="projectId">Project id of the project</param>
        /// <returns>204 no content</returns>
        [HttpGet("[controller]/{projectId}/-/create-profile")]
        public IActionResult CreateApplicationProfile(Guid projectId)
        {
            var project = _projectModel.GetById(projectId);

            if (project == null)
            {
                return NotFound($@"The project ""{projectId}"" was not found.");
            }

            var user = _authenticator.GetUser();

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - ???
            if (!_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                return Unauthorized("You are not an owner of the project.");
            }

            LogAnalyticsCreateApplicationProfile(project, user);

            return NoContent();
        }

        /// <summary>
        /// Creates and sends a project invitation to a specified email.
        /// </summary>
        /// <param name="sendInvitationObject">Informations for sending an invitation. The invitation token is stored inside the URL under ".../?invitationToken={token}".</param>
        /// <returns>NoContent</returns>
        [HttpPost("[controller]/invitation")]
        public IActionResult SendInvitation([FromBody] SendInvitationObject sendInvitationObject)
        {
            var user = _authenticator.GetUser();

            if (!IsValidEmail(sendInvitationObject.Email))
            {
                return BadRequest($@"The email ""{sendInvitationObject.Email}"" is invalid.");
            }

            var project = _projectModel.GetById(sendInvitationObject.Project);

            if (project == null)
            {
                return NotFound($@"The project ""{sendInvitationObject.Project}"" was not found.");
            }

            if (_roleModel.GetById(sendInvitationObject.Role) == null)
            {
                return NotFound($@"The role ""{sendInvitationObject.Role}"" was not found.");
            }

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Change Settings (project, user, quota)
            if (!_projectModel.HasAccess(user, project, UserRoles.Owner))
            {
                return Unauthorized("You are not an owner of the project.");
            }

            var invitations = _invitationModel.GetAllWhere(
                x => x.Project == sendInvitationObject.Project &&
                     x.InviteeEmail == sendInvitationObject.Email &&
                     x.Expiration > DateTime.UtcNow
            );

            if (invitations.Any())
            {
                return BadRequest("This invitee already has a valid invitation to this project.");
            }

            var expiredInvitations = _invitationModel.GetAllWhere(
                x => x.Project == sendInvitationObject.Project &&
                     x.InviteeEmail == sendInvitationObject.Email &&
                     x.Expiration <= DateTime.UtcNow
            );

            foreach (var expiredInvitation in expiredInvitations)
            {
                _invitationModel.Delete(expiredInvitation);
            }

            var token = _invitationModel.CreateInvitation(sendInvitationObject.Project, user.Id, sendInvitationObject.Role, sendInvitationObject.Email);

            var body = new JObject
            {
                ["Args"] = new JObject()
                {
                    ["placeholder"] = new JObject()
                    {
                        ["confirmation_link"] = $"{_configuration.GetString("coscine/local/api/additional/url")}/?invitationToken={token}"
                    }
                }
            };

            NotificationBusUtil.Send(Program.Configuration, "user_invitation", NotificationBusUtil.GetUserList(new User { EmailAddress = sendInvitationObject.Email }), sendInvitationObject.Project.ToString(), body);

            if (Request.Query != null && Request.Query["noanalyticslog"] != "true")
            {
                LogAnalyticsInviteExternalUser(project, user);
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a project invitation.
        /// </summary>
        /// <param name="invitationId">Id of an invitation</param>
        /// <returns>NoContent</returns>
        [HttpDelete("[controller]/invitation/{invitationId}")]
        public IActionResult DeleteInvitation(Guid invitationId)
        {
            var invitation = _invitationModel.GetById(invitationId);

            if (invitation == null)
            {
                return NotFound("Invitation was not found.");
            }

            var user = _authenticator.GetUser();

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Change Settings (project, user, quota)
            if (!_projectModel.HasAccess(user, _projectModel.GetById(invitation.Project), UserRoles.Owner))
            {
                return Unauthorized("You are not an owner of this project.");
            }

            _invitationModel.Delete(invitation);

            return NoContent();
        }

        /// <summary>
        /// Resolves a project invitation for the current user.
        /// </summary>
        /// <param name="invitationToken">Token for a project invitation stored inside the URL under ".../?invitationToken={token}"</param>
        /// <returns>NoContent</returns>
        [HttpGet("[controller]/invitation/resolve/{invitationToken}")]
        public IActionResult ResolveInvitation(Guid invitationToken)
        {
            var user = _authenticator.GetUser();

            var invitation = _invitationModel.GetByToken(invitationToken);

            if (invitation == null)
            {
                return NotFound("Invitation was not found.");
            }

            if (invitation.Expiration < DateTime.UtcNow)
            {
                return BadRequest("The invitation has expired");
            }

            var project = _projectModel.GetById(invitation.Project);

            // Rights Matrix (https://git.rwth-aachen.de/coscine/docs/private/internal-wiki/-/blob/master/coscine/Definition%20of%20rights%20Matrix.md)
            // - Project: Change Settings (project, user, quota)
            if (!_projectModel.HasAccess(_userModel.GetById(invitation.Issuer), project, UserRoles.Owner))
            {
                return Unauthorized("The issuer is not an owner of the project.");
            }

            if (_projectRoleModel.GetAllWhere(x => x.ProjectId == invitation.Project && x.UserId == user.Id).Any())
            {
                return BadRequest("The invitee is already part of the project.");
            }

            var role = _roleModel.GetById(invitation.Role);

            _emitter.EmitUserAdd(new UserEventArgs(_configuration)
            {
                Project = project,
                Role = role,
                User = user,
            });

            var projectRole = new ProjectRole()
            {
                RelationId = Guid.NewGuid(),
                ProjectId = invitation.Project,
                UserId = user.Id,
                RoleId = invitation.Role
            };

            _projectRoleModel.Insert(projectRole);

            _invitationModel.Delete(invitation);

            return NoContent();
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email).Address == email;
            }
            catch
            {
                return false;
            }
        }

        private async Task LogAnalyticsViewHome(List<string> projectIds)
        {
            await Task.Run(() =>
            {
                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "View Home",
                        ProjectList = projectIds
                    });
            });
        }

        private async Task LogAnalyticsViewProject(Database.DataModel.Project project, List<ResourceObject> resources, IEnumerable<DisciplineObject> disciplines, IEnumerable<OrganizationObject> organizations, User user)
        {
            await Task.Run(() =>
            {
                var enabledResources = ResourceTypeFactory.Instance.GetResourceTypes();
                var resourceTypes = _resourceTypeModel.GetAllWhere(r => enabledResources.Any(e => r.SpecificType.Equals(e)));

                var objects = resourceTypes.Select(x => Helpers.CreateProjectQuotaReturnObject(x, project.Id));

                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "View Project",
                        RoleId = _projectRoleModel.GetGetUserRoleForProject(project.Id, user.Id).ToString(),
                        ProjectId = project.Id.ToString(),
                        QuotaSize = objects.Select(x => $"{x.Name}: {x.Allocated.Value}/{x.Maximum.Value}").ToList(),
                        Disciplines = disciplines.Select(x => x.DisplayNameEn).ToList(),
                        Organizations = organizations.Select(x => x.DisplayName).ToList(),
                        Visibility = project.VisibilityId.HasValue ? _visibilityModel.GetById(project.VisibilityId.Value)?.DisplayName : null,
                        ResourceList = resources.ConvertAll(x => x.Id.ToString()),
                    });
            });
        }

        private async Task LogAnalyticsEditProject(Database.DataModel.Project project, string metadataCompletness, IEnumerable<DisciplineObject> disciplines, IEnumerable<OrganizationObject> organizations, User user)
        {
            await Task.Run(() =>
            {
                var resourceTypes = _resourceTypeModel.GetAllWhere(x => x.Enabled == true);

                var objects = resourceTypes.Select(x => Helpers.CreateProjectQuotaReturnObject(x, project.Id));

                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "Edit Project",
                        RoleId = _projectRoleModel.GetGetUserRoleForProject(project.Id, user.Id).ToString(),
                        ProjectId = project.Id.ToString(),
                        QuotaSize = objects.Select(x => $"{x.Name}: {x.Allocated.Value}/{x.Maximum.Value}").ToList(),
                        MetadataCompleteness = metadataCompletness,
                        Disciplines = disciplines.Select(x => x.DisplayNameEn).ToList(),
                        Organizations = organizations.Select(x => x.DisplayName).ToList(),
                        Visibility = project.VisibilityId.HasValue ? _visibilityModel.GetById(project.VisibilityId.Value)?.DisplayName : null,
                    });
            });
        }

        private async Task LogAnalyticsAddProject(Database.DataModel.Project project, string metadataCompletness, IEnumerable<DisciplineObject> disciplines, IEnumerable<OrganizationObject> organizations, User user)
        {
            await Task.Run(() =>
            {
                var resourceTypes = _resourceTypeModel.GetAllWhere(x => x.Enabled == true);

                var objects = resourceTypes.Select(x => Helpers.CreateProjectQuotaReturnObject(x, project.Id));

                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "Add Project",
                        RoleId = _projectRoleModel.GetGetUserRoleForProject(project.Id, user.Id).ToString(),
                        ProjectId = project.Id.ToString(),
                        QuotaSize = objects.Select(x => $"{x.Name}: {x.Allocated.Value}/{x.Maximum.Value}").ToList(),
                        MetadataCompleteness = metadataCompletness,
                        Disciplines = disciplines.Select(x => x.DisplayNameEn).ToList(),
                        Organizations = organizations.Select(x => x.DisplayName).ToList(),
                        Visibility = project.VisibilityId.HasValue ? _visibilityModel.GetById(project.VisibilityId.Value)?.DisplayName : null,
                    });
            });
        }

        private async Task LogAnalyticsDeleteProject(Database.DataModel.Project project, IEnumerable<DisciplineObject> disciplines, IEnumerable<OrganizationObject> organizations, User user)
        {
            await Task.Run(() =>
            {
                var resourceTypes = _resourceTypeModel.GetAllWhere(x => x.Enabled == true);

                var objects = resourceTypes.Select(x => Helpers.CreateProjectQuotaReturnObject(x, project.Id));

                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "Delete Project",
                        RoleId = _projectRoleModel.GetGetUserRoleForProject(project.Id, user.Id).ToString(),
                        ProjectId = project.Id.ToString(),
                        QuotaSize = objects.Select(x => $"{x.Name}: {x.Allocated.Value}/{x.Maximum.Value}").ToList(),
                        Disciplines = disciplines.Select(x => x.DisplayNameEn).ToList(),
                        Organizations = organizations.Select(x => x.DisplayName).ToList(),
                        Visibility = project.VisibilityId.HasValue ? _visibilityModel.GetById(project.VisibilityId.Value)?.DisplayName : null,
                    });
            });
        }

        private async Task LogAnalyticsInviteExternalUser(Database.DataModel.Project project, User user)
        {
            await Task.Run(() =>
            {
                var resourceTypes = _resourceTypeModel.GetAllWhere(x => x.Enabled == true);

                var objects = resourceTypes.Select(x => Helpers.CreateProjectQuotaReturnObject(x, project.Id));

                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "Invite External User",
                        RoleId = _projectRoleModel.GetGetUserRoleForProject(project.Id, user.Id).ToString(),
                        ProjectId = project.Id.ToString()
                    });
            });
        }

        private async Task LogAnalyticsCreateApplicationProfile(Database.DataModel.Project project, User user)
        {
            await Task.Run(() =>
            {
                _coscineLogger.AnalyticsLog(
                new AnalyticsLogObject
                {
                    Type = "Action",
                    Operation = "Create Application Profile",
                    RoleId = _projectRoleModel.GetGetUserRoleForProject(project.Id, user.Id).ToString(),
                    ProjectId = project.Id.ToString()
                });
            });
        }
    }
}