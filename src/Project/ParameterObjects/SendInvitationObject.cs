﻿using Newtonsoft.Json;
using System;

namespace Coscine.Api.Project.ParameterObjects
{
    /// <summary>
    ///  Parameter object containing the invitation informations.
    /// </summary>
    public class SendInvitationObject
    {
        /// <summary>
        ///  Id of the project
        /// </summary>
        [JsonProperty("projectId")]
        public Guid Project { get; set; }

        /// <summary>
        ///  Id of the target role
        /// </summary>
        [JsonProperty("role")]
        public Guid Role { get; set; }

        /// <summary>
        ///  Email of the target user
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}