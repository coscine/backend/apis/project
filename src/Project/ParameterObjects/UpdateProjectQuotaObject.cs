﻿namespace Coscine.Api.Project.ParameterObjects
{
    /// <summary>
    ///  Parameter object containing the update informations.
    /// </summary>
    public class UpdateProjectQuotaObject
    {
        /// <summary>
        /// New quota that will be the current allocated value.
        /// </summary>
        public int AllocatedGiB { get; set; }
    }
}