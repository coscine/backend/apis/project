﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.Project
{
    /// <summary>
    /// Standard Program Class
    /// </summary>
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        /// <summary>
        /// Standard Main Method
        /// </summary>
        public static void Main()
        {
            InitializeWebService<Startup>();
        }
    }
}