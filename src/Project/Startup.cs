﻿using Coscine.ApiCommons;

namespace Coscine.Api.Project
{
    /// <summary>
    /// Standard Startup Class
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Standard Startup Constructor
        /// </summary>
        public Startup()
        {
        }
    }
}